# Phonograph Backend

## Installation

```bash
$ npm install
```

## Running the app

1. `npm install`
2. `docker-compose up`
3. create `.env` file using `.env.example`.
4. `npm run start:dev`.

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

