import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BotModule } from './bot/bot.module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { WsModule } from './ws/ws.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { SongModule } from './song/song.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    BotModule,
    SharedModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      url: process.env.DB_URL,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
      logging: false,
    }),
    AuthModule,
    WsModule,
    EventEmitterModule.forRoot(),
    SongModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
