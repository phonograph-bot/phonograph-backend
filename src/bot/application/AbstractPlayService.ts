import { IEventPublisher } from 'src/shared/domain/IEventPublisher';
import IBot from '../domain/IBot';
import { ISongDownloader } from '../domain/ISongDownloader';
import IUrlParser from '../domain/IUrlParser';
import IQueueRepository from '../domain/persistence/IQueueRepository';
import BotPlayService from '../domain/services/bot-play.service';
import QueueAddService from '../domain/services/queue-add.service';
import QueueDeleterService from '../domain/services/queue-deleter.service';
import QueueFinderService from '../domain/services/queue-finder.service';
import StreamService from '../domain/services/stream.service';
import UrlParserService from '../domain/services/url-parser.service';

export default abstract class AbstractPlayService {
  protected queueDelete: QueueDeleterService;
  protected queueFinder: QueueFinderService;
  protected queueAdd: QueueAddService;
  protected botPlayService: BotPlayService;
  protected urlParser: UrlParserService;
  protected streamService: StreamService;

  constructor(
    protected downloaders: ISongDownloader[],
    protected urlParsers: IUrlParser[],
    protected bot: IBot,
    protected queueRepository: IQueueRepository,
    protected eventPublisher: IEventPublisher,
  ) {
    this.queueDelete = new QueueDeleterService(queueRepository, eventPublisher);
    this.queueFinder = new QueueFinderService(queueRepository);
    this.queueAdd = new QueueAddService(queueRepository, eventPublisher);
    this.urlParser = new UrlParserService(urlParsers);
    this.streamService = new StreamService(downloaders);
    this.botPlayService = new BotPlayService(
      bot,
      this.streamService,
      this.queueFinder,
      this.queueDelete,
      this.eventPublisher,
    );
  }
}
