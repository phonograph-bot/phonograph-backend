import { Inject, Injectable } from '@nestjs/common';
import IBot from 'src/bot/domain/IBot';
import { Server } from 'src/bot/domain/models/Server';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import ServerFinderService from 'src/bot/domain/services/server-finder.service';
import { IEventPublisher } from 'src/shared/domain/IEventPublisher';
import { ServerStatusEvent } from 'src/bot/domain/events/ServerStatusEvent';
import { ServerStatusType } from 'src/bot/domain/models/ServerStatusType';

@Injectable()
export default class StopService {
  private serverFinder: ServerFinderService;

  constructor(
    @Inject('BOT') private bot: IBot,
    @Inject('EVENT') private eventPublisher: IEventPublisher,
    @Inject('ServerRepository') serverRepository: IServerRepository,
  ) {
    this.serverFinder = new ServerFinderService(serverRepository, bot);
  }

  async stop(serverId: string): Promise<void> {
    const server = await this.serverFinder.find(serverId);
    this.publishEventState(server);
    this.bot.disconnect(server);
  }

  private publishEventState(server: Server) {
    const statusEvent = new ServerStatusEvent(server.id, {
      minute: 0,
      status: ServerStatusType.Stopped,
    });
    this.eventPublisher.publish(statusEvent);
  }
}
