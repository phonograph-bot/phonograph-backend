import { Inject, Injectable } from '@nestjs/common';
import IBot from 'src/bot/domain/IBot';
import Message from '../../domain/models/Message';
import ChannelCheckerService from 'src/bot/domain/services/channel-checker.service';

@Injectable()
export class PingService {
  constructor(
    @Inject('BOT') private bot: IBot,
    private channelChecker: ChannelCheckerService,
  ) {}

  ping(channelId: string, content: string) {
    this.channelChecker.check(this.bot, channelId);

    const message = Message.create(channelId, content);
    this.bot.writeMessage(message);
  }
}
