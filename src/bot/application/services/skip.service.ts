import { Inject, Injectable } from '@nestjs/common';
import IBot from 'src/bot/domain/IBot';
import { ISongDownloader } from 'src/bot/domain/ISongDownloader';
import IUrlParser from 'src/bot/domain/IUrlParser';
import IQueueRepository from 'src/bot/domain/persistence/IQueueRepository';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import ServerFinderService from 'src/bot/domain/services/server-finder.service';
import { EventPublisher } from 'src/shared/infrastructure/EventPublisher';
import AbstractPlayService from '../AbstractPlayService';

@Injectable()
export class SkipService extends AbstractPlayService {
  private serverFinder: ServerFinderService;

  constructor(
    @Inject('UrlParsers') parsers: IUrlParser[],
    @Inject('SongDownloaders') downloaders: ISongDownloader[],
    @Inject('BOT') bot: IBot,
    @Inject('ServerRepository') serverRepository: IServerRepository,
    @Inject('QueueRepository') queueRepository: IQueueRepository,
    @Inject('EVENT') eventPublisher: EventPublisher,
  ) {
    super(downloaders, parsers, bot, queueRepository, eventPublisher);
    this.serverFinder = new ServerFinderService(serverRepository, bot);
  }

  async skip(serverId: string): Promise<void> {
    const server = await this.serverFinder.find(serverId);

    const channelId = this.bot.getCurrentChannel(server);
    if (!channelId) {
      return;
    }

    const lastSong = await this.queueFinder.findFirst(server);
    if (!lastSong) {
      return;
    }

    await this.queueDelete.delete(lastSong);

    if (this.bot.isConnected(server)) {
      this.botPlayService.play(server, channelId);
    }
  }
}
