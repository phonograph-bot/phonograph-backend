import { Inject, Injectable } from '@nestjs/common';
import IBot from 'src/bot/domain/IBot';
import { ISongDownloader } from 'src/bot/domain/ISongDownloader';
import IUrlParser from 'src/bot/domain/IUrlParser';
import IQueueRepository from 'src/bot/domain/persistence/IQueueRepository';
import AbstractPlayService from '../AbstractPlayService';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import ServerFinderService from 'src/bot/domain/services/server-finder.service';
import { IEventPublisher } from 'src/shared/domain/IEventPublisher';

@Injectable()
export class TimeService extends AbstractPlayService {
  private serverFinder: ServerFinderService;

  constructor(
    @Inject('UrlParsers') parsers: IUrlParser[],
    @Inject('SongDownloaders') downloaders: ISongDownloader[],
    @Inject('BOT') bot: IBot,
    @Inject('QueueRepository') queueRepository: IQueueRepository,
    @Inject('ServerRepository') serverRepository: IServerRepository,
    @Inject('EVENT') eventPublisher: IEventPublisher,
  ) {
    super(downloaders, parsers, bot, queueRepository, eventPublisher);
    this.serverFinder = new ServerFinderService(serverRepository, bot);
  }

  async setTime(serverId: string, minute: number) {
    const channelId = this.bot.findVoiceChannelByUserConnected(serverId);
    const server = await this.serverFinder.find(serverId);

    return this.botPlayService.play(server, channelId, minute);
  }
}
