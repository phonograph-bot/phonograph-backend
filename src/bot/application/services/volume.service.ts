import { Inject, Injectable } from '@nestjs/common';
import { ServerEvent } from 'src/bot/domain/events/ServerEvent';
import IBot from 'src/bot/domain/IBot';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import ServerFinderService from 'src/bot/domain/services/server-finder.service';
import ServerUpdaterService from 'src/bot/domain/services/server-updater.service';
import { IEventPublisher } from 'src/shared/domain/IEventPublisher';

@Injectable()
export default class VolumeService {
  private serverUpdater: ServerUpdaterService;
  private serverFinder: ServerFinderService;

  constructor(
    @Inject('BOT') private bot: IBot,
    @Inject('EVENT') private eventPublisher: IEventPublisher,
    @Inject('ServerRepository') serverRepository: IServerRepository,
  ) {
    this.serverFinder = new ServerFinderService(serverRepository, bot);
    this.serverUpdater = new ServerUpdaterService(serverRepository);
  }

  async setVolume(serverId: string, volume: number) {
    const server = await this.serverFinder.find(serverId);

    server.volume = volume;
    await this.serverUpdater.update(server);
    this.bot.setVolume(server);

    this.eventPublisher.publish(new ServerEvent(server));
  }
}
