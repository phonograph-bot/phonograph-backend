import { Module } from '@nestjs/common';
import { DiscordModule as DiscordBotModule } from 'discord-nestjs';
import DiscordBot from './infrastructure/DiscordJsBot';
import { PingGateway } from './infrastructure/gateways/ping';
import { PingService } from './application/services/ping.service';
import { PlayGateway } from './infrastructure/gateways/play';
import { YoutubeDownloader } from './infrastructure/services/YoutubeDownloader';
import { PlayUrlService } from './application/services/play-url.service';
import urlParsersFactory from './domain/factories/urlParsersFactory';
import songDownloadersFactory from './domain/factories/songDownloadersFactory';
import YoutubeUrlParser from './infrastructure/parsers/YoutubeUrlParser';
import ChannelCheckerService from './domain/services/channel-checker.service';
import ServerEntity from './infrastructure/persistence/TypeOrmRepository/entities/server.entity';
import SongEntity from './infrastructure/persistence/TypeOrmRepository/entities/song.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import TypeOrmServerRepository from './infrastructure/persistence/TypeOrmRepository/TypeOrmServerRepository';
import TypeOrmSongServerRepository from './infrastructure/persistence/TypeOrmRepository/TypeOrmSongServerRepository';
import SongServerEntity from './infrastructure/persistence/TypeOrmRepository/entities/song-server.entity';
import { SkipService } from './application/services/skip.service';
import { SkipGateway } from './infrastructure/gateways/skip';
import { VolumeGateway } from './infrastructure/gateways/volume';
import VolumeService from './application/services/volume.service';
import { PlayForceService } from './application/services/play-force.service';
import { PlayForceGateway } from './infrastructure/gateways/force';
import StopService from './application/services/stop.service';
import { StopGateway } from './infrastructure/gateways/stop';
import { PlayService } from './application/services/play.service';
import { SharedModule } from 'src/shared/shared.module';
import { PauseService } from './application/services/pause.service';
import { PauseGateway } from './infrastructure/gateways/pause';
import { ResumeService } from './application/services/resume.service';
import { ResumeGateway } from './infrastructure/gateways/resume';
import { TimeService } from './application/services/time.service';

const parsers = [YoutubeUrlParser];
const downloaders = [YoutubeDownloader];

@Module({
  imports: [
    DiscordBotModule.forRootAsync({
      useFactory: () => ({
        token: process.env.DISCORD_BOT_TOKEN,
        commandPrefix: process.env.DISCORD_COMMAND_PREFIX,
      }),
    }),
    TypeOrmModule.forFeature([ServerEntity, SongEntity, SongServerEntity]),
    SharedModule,
  ],
  providers: [
    ChannelCheckerService,
    YoutubeUrlParser,
    YoutubeDownloader,
    {
      provide: 'BOT',
      useClass: DiscordBot,
    },
    {
      provide: 'UrlParsers',
      useFactory: urlParsersFactory,
      inject: parsers,
    },
    {
      provide: 'SongDownloaders',
      useFactory: songDownloadersFactory,
      inject: downloaders,
    },
    {
      provide: 'ServerRepository',
      useClass: TypeOrmServerRepository,
    },
    {
      provide: 'QueueRepository',
      useClass: TypeOrmSongServerRepository,
    },
    PingService,
    PingGateway,
    PlayUrlService,
    PlayService,
    PlayGateway,
    SkipService,
    SkipGateway,
    VolumeService,
    VolumeGateway,
    PlayForceService,
    PlayForceGateway,
    StopService,
    StopGateway,
    PauseService,
    PauseGateway,
    ResumeService,
    ResumeGateway,
    TimeService,
  ],
  exports: [
    PauseService,
    ResumeService,
    PlayService,
    PlayUrlService,
    SkipService,
    VolumeService,
    TimeService,
    {
      provide: 'BOT',
      useClass: DiscordBot,
    },
    {
      provide: 'ServerRepository',
      useClass: TypeOrmServerRepository,
    },
    {
      provide: 'QueueRepository',
      useClass: TypeOrmSongServerRepository,
    },
  ],
})
export class BotModule {}
