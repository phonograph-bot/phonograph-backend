import { Injectable } from '@nestjs/common';
import { DiscordClientProvider } from 'discord-nestjs/dist/core/provider/discord-client-provider';
import { Client, TextChannel, VoiceChannel, VoiceConnection } from 'discord.js';
import { Readable } from 'node:stream';
import Message from 'src/bot/domain/models/Message';
import IBot from '../domain/IBot';
import { Server } from '../domain/models/Server';
import { ServerStatusType } from '../domain/models/ServerStatusType';

@Injectable()
export default class DiscordBot implements IBot {
  private client: Client;

  constructor(private readonly discordProvider: DiscordClientProvider) {
    this.client = this.discordProvider.getClient();
  }
  getStatus(server: Server): ServerStatusType {
    const dispatcher = this.findVoiceConnection(server)?.dispatcher;
    const loading = dispatcher?.['loading'];

    if (!dispatcher) {
      return ServerStatusType.Stopped;
    }

    if (loading) {
      return ServerStatusType.Loading;
    }

    if (dispatcher.paused) {
      return ServerStatusType.Paused;
    }

    return ServerStatusType.Playing;
  }

  pause(server: Server): void {
    const connection = this.findVoiceConnection(server);
    connection?.dispatcher?.pause();
  }

  resume(server: Server, onResume): void {
    const connection = this.findVoiceConnection(server);
    // drain detect the exact time when music starts
    connection?.dispatcher?.once('drain', onResume);
    connection?.dispatcher?.resume();
  }

  findServerByUserConnected(userId: string): string | null {
    const channels: VoiceChannel[] = <any>(
      this.client.channels.cache.filter((channel) => channel.type == 'voice')
    );

    const voiceChannel = channels.find((channel) =>
      channel.members.some((member) => member.id === userId),
    );

    return voiceChannel?.guild?.id;
  }

  findVoiceChannelByUserConnected(userId: string): string {
    const channels: VoiceChannel[] = <any>(
      this.client.channels.cache.filter((channel) => channel.type == 'voice')
    );

    const voiceChannel = channels.find((channel) =>
      channel.members.some((member) => member.id === userId),
    );

    return voiceChannel?.id;
  }

  checkServer(serverId: string): boolean {
    return !!this.client.guilds.cache.find((guild) => guild.id === serverId);
  }

  setVolume(server: Server): void {
    this.findVoiceConnection(server)?.voice?.connection?.dispatcher?.setVolume(
      server.volume / 100,
    );
  }

  getTime(server: Server): number {
    const connection = this.findVoiceConnection(server);
    const seek = connection?.dispatcher?.['seek'] | 0;
    return connection?.dispatcher?.streamTime / 1000 + seek || 0;
  }

  checkChannel(channelId: string): boolean {
    return !!(
      this.findTextChannel(channelId) || this.findVoiceChannel(channelId)
    );
  }

  async writeMessage(message: Message): Promise<void> {
    const channel: TextChannel = await this.findTextChannel(message.channelId);
    channel.send(message.content);
  }

  getCurrentChannel(server: Server): string {
    return this.client.guilds.cache.find((guild) => guild.id === server.id)
      ?.voice?.channelID;
  }

  async play(
    stream: Readable,
    server: Server,
    onNextSong: () => void,
    onStart: () => void,
    onLoading: () => void,
    minute: number,
    channelId?: string,
  ): Promise<void> {
    let voiceChannel = this.findVoiceChannel(channelId);
    if (!voiceChannel) {
      voiceChannel = this.findVoiceConnection(server).channel;
    }

    const connection = await voiceChannel.join();
    const dispatcher = connection.play(stream, {
      volume: server.volume / 100,
      seek: minute,
    });

    onLoading();
    dispatcher['loading'] = true;
    dispatcher['seek'] = minute;

    dispatcher.once('finish', onNextSong);
    // Start wait for data loading
    dispatcher.once('start', () => {
      dispatcher['loading'] = false;
      onStart();
    });
  }

  isConnected(server: Server): boolean {
    return !!this.findVoiceConnection(server);
  }

  disconnect(server: Server): void {
    this.client.voice.connections
      .find((s) => s.channel.guild.id === server.id)
      ?.disconnect();
  }

  private findTextChannel(channelId: string | number): TextChannel | null {
    return <TextChannel>(
      this.client.channels.cache.find(
        (channel) => channel.id === channelId && channel.isText(),
      )
    );
  }

  private findVoiceChannel(channelId: string): VoiceChannel | null {
    return <VoiceChannel>(
      this.client.guilds.client.voice.client.channels.cache.find(
        (channel) => channel.id === channelId,
      )
    );
  }

  private findVoiceConnection(server: Server): VoiceConnection | null {
    return this.client.voice.connections.find(
      (connection) => connection.channel.guild.id === server.id,
    );
  }
}
