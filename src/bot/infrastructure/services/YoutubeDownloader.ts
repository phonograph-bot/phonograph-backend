import { Injectable } from '@nestjs/common';
import { Readable } from 'node:stream';
import { ISongDownloader } from 'src/bot/domain/ISongDownloader';
import { Song } from 'src/bot/domain/models/Song';
import { SongType } from 'src/bot/domain/models/SongType';
import * as ytdl from 'discord-ytdl-core';

@Injectable()
export class YoutubeDownloader implements ISongDownloader {
  supports(song: Song): boolean {
    return song.type === SongType.Youtube;
  }

  getStream(song: Song): Readable {
    return ytdl(song.url, {
      filter: 'audioonly',
      fmt: 'mp3',
    });
  }
}
