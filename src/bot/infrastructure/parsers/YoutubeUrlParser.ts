import { Injectable } from '@nestjs/common';
import IUrlParser from 'src/bot/domain/IUrlParser';
import { Song } from 'src/bot/domain/models/Song';
import { SongType } from 'src/bot/domain/models/SongType';
import * as ytdl from 'ytdl-core';

@Injectable()
export default class YoutubeUrlParser implements IUrlParser {
  private YOUTUBE = /^http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?$/;

  supports(url: string): boolean {
    return this.YOUTUBE.test(url);
  }
  async parse(url: string): Promise<Song> {
    const song = await ytdl.getInfo(url);
    const title = song.videoDetails.title;
    const minutes = Number(song.videoDetails.lengthSeconds);
    const author = song.videoDetails.author.name;
    const image = song.videoDetails.thumbnails[0].url;

    return Song.create(url, SongType.Youtube, minutes, title, author, image);
  }
}
