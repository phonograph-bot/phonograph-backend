import { InjectRepository } from '@nestjs/typeorm';
import { Server } from 'src/bot/domain/models/Server';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import { Repository } from 'typeorm';
import ServerEntity from './entities/server.entity';
import ServerMap from './mapper/ServerMap';

export default class TypeOrmServerRepository implements IServerRepository {
  constructor(
    @InjectRepository(ServerEntity)
    private serverRepo: Repository<ServerEntity>,
  ) {}

  async save(server: Server): Promise<void> {
    const serverEntity = ServerMap.toPersistence(server);
    await this.serverRepo.save(serverEntity);
  }

  findById(id: string): Promise<Server | null> {
    if (!id) {
      return null;
    }

    return this.serverRepo.findOne(id).then((serverEntity) => {
      return serverEntity ? ServerMap.toDomain(serverEntity) : null;
    });
  }
}
