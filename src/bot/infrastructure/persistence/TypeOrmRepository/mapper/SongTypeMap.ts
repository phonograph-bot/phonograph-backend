import { SongType } from 'src/bot/domain/models/SongType';
import { SongTypeEnum } from '../entities/song.enum';

export default class SongTypeMap {
  static toPersistence(type: SongType): SongTypeEnum {
    return type.toString() as SongTypeEnum;
  }

  static toDomain(songEntity: SongTypeEnum): SongType {
    return songEntity.toString() as SongType;
  }
}
