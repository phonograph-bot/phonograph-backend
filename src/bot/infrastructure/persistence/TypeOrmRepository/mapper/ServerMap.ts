import { Server } from 'src/bot/domain/models/Server';
import ServerEntity from '../entities/server.entity';

export default class ServerMap {
  static toPersistence(server: Server): ServerEntity {
    const serverEntity = new ServerEntity();
    serverEntity.id = server.id;
    serverEntity.volume = server.volume;
    return serverEntity;
  }

  static toDomain(serverEntity: ServerEntity): Server {
    const server = new Server(serverEntity.id, serverEntity.volume);
    return server;
  }
}
