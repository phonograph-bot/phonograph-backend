import { Song } from 'src/bot/domain/models/Song';
import SongEntity from '../entities/song.entity';
import SongTypeMap from './SongTypeMap';

export default class SongMap {
  static toPersistence(song: Song): SongEntity {
    const songEntity = new SongEntity();
    songEntity.url = song.url;
    songEntity.type = SongTypeMap.toPersistence(song.type);
    songEntity.minute = song.minute;
    songEntity.title = song.title;
    songEntity.author = song.author;
    songEntity.image = song.image;
    return songEntity;
  }

  static toDomain(songEntity: SongEntity): Song {
    return new Song(
      songEntity.url,
      SongTypeMap.toDomain(songEntity.type),
      songEntity.minute,
      songEntity.title,
      songEntity.author,
      songEntity.image,
    );
  }
}
