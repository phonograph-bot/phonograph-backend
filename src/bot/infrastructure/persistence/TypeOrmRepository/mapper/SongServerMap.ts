import SongQueued from 'src/bot/domain/models/SongQueued';
import SongServerEntity from '../entities/song-server.entity';
import ServerMap from './ServerMap';
import SongMap from './SongMap';

export default class SongServerMap {
  static toPersistence(songServer: SongQueued): SongServerEntity {
    const song = SongMap.toPersistence(songServer.song);
    const server = ServerMap.toPersistence(songServer.server);

    const songServerEntity = new SongServerEntity();
    songServerEntity.server = server;
    songServerEntity.song = song;
    songServerEntity.order = songServer.order;

    return songServerEntity;
  }

  static toDomain(songServerEntity: SongServerEntity): SongQueued {
    const song = SongMap.toDomain(songServerEntity.song);
    const server = ServerMap.toDomain(songServerEntity.server);

    const songServer = new SongQueued(songServerEntity.order, server, song);
    return songServer;
  }
}
