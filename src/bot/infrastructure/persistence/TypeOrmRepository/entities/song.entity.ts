import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import SongServerEntity from './song-server.entity';
import { SongTypeEnum } from './song.enum';

@Entity()
export default class SongEntity {
  @PrimaryColumn({
    length: 500,
  })
  url: string;

  @Column({
    type: 'enum',
    enum: SongTypeEnum,
  })
  type: SongTypeEnum;

  @OneToMany(
    () => SongServerEntity,
    (songServerEntity) => songServerEntity.song,
  )
  queue: SongServerEntity[];

  @Column()
  minute: number;

  @Column()
  title: string;

  @Column()
  author: string;

  @Column()
  image: string;
}
