import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import SongEntity from './song.entity';

@Entity()
export default class ServerEntity {
  @PrimaryColumn()
  id: string;

  @Column()
  volume: number;

  @OneToMany(() => SongEntity, (songEntity) => songEntity.url)
  queue: SongEntity[];
}
