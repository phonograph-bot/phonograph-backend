import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import ServerEntity from './server.entity';
import SongEntity from './song.entity';

@Entity()
export default class SongServerEntity {
  @PrimaryColumn()
  serverId: string;

  @PrimaryColumn()
  order: number;

  @ManyToOne(() => ServerEntity, {
    cascade: ['insert'],
  })
  server: ServerEntity;

  @ManyToOne(() => SongEntity, {
    cascade: ['insert'],
  })
  @JoinColumn({ name: 'songUrl' })
  song: SongEntity;
}
