import { InjectRepository } from '@nestjs/typeorm';
import { Song } from 'src/bot/domain/models/Song';
import ISongRepository from 'src/bot/domain/persistence/ISongRepository';
import { Repository } from 'typeorm';
import SongEntity from './entities/song.entity';
import SongMap from './mapper/SongMap';

export default class TypeOrmSongRepository implements ISongRepository {
  constructor(
    @InjectRepository(SongEntity)
    private songRepo: Repository<SongEntity>,
  ) {}

  save(song: Song): void {
    const songEntity = SongMap.toPersistence(song);
    this.songRepo.save(songEntity);
  }
  findById(id: string): Promise<Song | null> {
    if (!id) {
      return null;
    }

    return this.songRepo
      .findOne(id)
      .then((songEntity) => (songEntity ? SongMap.toDomain(songEntity) : null));
  }
}
