import { InjectRepository } from '@nestjs/typeorm';
import { Server } from 'src/bot/domain/models/Server';
import SongQueued from 'src/bot/domain/models/SongQueued';
import Criteria from 'src/bot/domain/persistence/Criteria';
import IQueueRepository from 'src/bot/domain/persistence/IQueueRepository';
import { getConnection, Repository } from 'typeorm';
import SongServerEntity from './entities/song-server.entity';
import SongEntity from './entities/song.entity';
import SongServerMap from './mapper/SongServerMap';

export default class TypeOrmSongServerRepository implements IQueueRepository {
  constructor(
    @InjectRepository(SongServerEntity)
    private songServerRepo: Repository<SongServerEntity>,
    @InjectRepository(SongEntity)
    private songRepo: Repository<SongEntity>,
  ) {}

  find(criteria: Criteria, limit: number, asc: boolean): Promise<SongQueued[]> {
    return this.songServerRepo
      .createQueryBuilder('songServer')
      .where('songServer.serverId = :serverId', {
        serverId: criteria.server.id,
      })
      .andWhere(criteria.order ? 'songServer.order = :order' : '1=1', {
        order: criteria.order,
      })
      .orderBy('songServer.order', asc ? 'ASC' : 'DESC')
      .leftJoinAndSelect('songServer.server', 'server')
      .leftJoinAndSelect('songServer.song', 'song')
      .limit(limit)
      .getMany()
      .then((songServers) => {
        return songServers.map((songServer) =>
          SongServerMap.toDomain(songServer),
        );
      });
  }

  async delete(server: Server, order: number): Promise<void> {
    await this.songServerRepo.delete({
      order,
      serverId: server.id,
    });
  }

  async save(songServer: SongQueued): Promise<void> {
    return getConnection().transaction(async (transactionalEntityManager) => {
      const songServerEntity = SongServerMap.toPersistence(songServer);
      await this.songServerRepo
        .createQueryBuilder()
        .update(SongServerEntity)
        .set({ order: () => '`order` + 1' })
        .where('`order` >= :order', { order: songServer.order })
        .orderBy('order', 'DESC')
        .execute();

      await transactionalEntityManager.save(songServerEntity);
    });
  }

  async saveLast(songServer: SongQueued): Promise<void> {
    const songQueued = SongServerMap.toPersistence(songServer);
    await this.songRepo.save(songQueued.song);

    await this.songServerRepo.query(
      'INSERT INTO `song_server_entity`(`serverId`, `order`, `songUrl`) (SELECT ?, IFNULL(MAX(song_server_entity.order) + 1, 0), ? FROM song_server_entity)',
      [songQueued.server.id, songQueued.song.url],
    );
  }
}
