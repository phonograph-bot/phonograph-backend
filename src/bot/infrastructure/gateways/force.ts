import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { PlayForceService } from 'src/bot/application/services/play-force.service';
import { InvalidUrlException } from 'src/bot/domain/exceptions/InvalidUrlException';
import { ChannelNotFoundException } from '../../domain/exceptions/ChannelNotFoundException';

@Injectable()
export class PlayForceGateway {
  constructor(private playService: PlayForceService) {}

  @OnCommand({
    name: 'force',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onPing(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    try {
      await this.playService.play(
        content,
        context.member.voice?.channel?.id,
        context.guild.id,
      );
    } catch (error) {
      if (error instanceof InvalidUrlException) {
        await context.reply(error.message);
        return;
      }
      if (error instanceof ChannelNotFoundException) {
        await context.reply('connect to voice channel');
        return;
      }

      throw error;
    }
  }
}
