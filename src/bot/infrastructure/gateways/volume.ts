import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import VolumeService from 'src/bot/application/services/volume.service';
import { ServerException } from 'src/bot/domain/exceptions/ServerException';

@Injectable()
export class VolumeGateway {
  constructor(private volumeService: VolumeService) {}

  @OnCommand({
    name: 'volume',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onSkip(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    try {
      await this.volumeService.setVolume(
        context.member.guild.id,
        parseInt(content),
      );
    } catch (error) {
      if (error instanceof ServerException) {
        await context.reply('volume invalid');
        return;
      }

      throw error;
    }
  }
}
