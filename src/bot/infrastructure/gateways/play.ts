import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { PlayUrlService } from 'src/bot/application/services/play-url.service';
import { PlayService } from 'src/bot/application/services/play.service';
import { InvalidUrlException } from 'src/bot/domain/exceptions/InvalidUrlException';
import { ChannelNotFoundException } from '../../domain/exceptions/ChannelNotFoundException';

@Injectable()
export class PlayGateway {
  constructor(
    private playUrlService: PlayUrlService,
    private playService: PlayService,
  ) {}

  @OnCommand({
    name: 'play',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onPing(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    const channelId = context.member.voice?.channel?.id;
    const serverId = context.guild.id;

    try {
      if (!content) {
        await this.playService.play(channelId, serverId);
      } else {
        await this.playUrlService.play(content, channelId, serverId);
      }
    } catch (error) {
      if (error instanceof InvalidUrlException) {
        await context.reply(error.message);
        return;
      }
      if (error instanceof ChannelNotFoundException) {
        await context.reply('connect to voice channel');
        return;
      }

      throw error;
    }
  }
}
