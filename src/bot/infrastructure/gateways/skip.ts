import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { SkipService } from 'src/bot/application/services/skip.service';

@Injectable()
export class SkipGateway {
  constructor(private skipService: SkipService) {}

  @OnCommand({
    name: 'skip',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onSkip(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    return this.skipService.skip(context.member.guild.id);
  }
}
