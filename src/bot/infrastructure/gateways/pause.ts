import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { PauseService } from 'src/bot/application/services/pause.service';

@Injectable()
export class PauseGateway {
  constructor(private pauseService: PauseService) {}

  @OnCommand({
    name: 'pause',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onPause(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    return this.pauseService.pause(context.member.guild.id);
  }
}
