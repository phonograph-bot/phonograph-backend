import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { ResumeService } from 'src/bot/application/services/resume.service';

@Injectable()
export class ResumeGateway {
  constructor(private resumeService: ResumeService) {}

  @OnCommand({
    name: 'resume',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onPause(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    return this.resumeService.resume(context.member.guild.id);
  }
}
