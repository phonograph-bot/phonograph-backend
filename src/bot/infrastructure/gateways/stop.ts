import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import StopService from 'src/bot/application/services/stop.service';

@Injectable()
export class StopGateway {
  constructor(private stopService: StopService) {}

  @OnCommand({
    name: 'stop',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onStop(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    return this.stopService.stop(context.member.guild.id);
  }
}
