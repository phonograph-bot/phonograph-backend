import { Injectable } from '@nestjs/common';
import { Content, Context, OnCommand } from 'discord-nestjs';
import { Message } from 'discord.js';
import { PingService } from 'src/bot/application/services/ping.service';
import { MessageException } from 'src/bot/domain/exceptions/MessageException';

@Injectable()
export class PingGateway {
  constructor(private pingService: PingService) {}

  @OnCommand({
    name: 'ping',
    isIgnoreBotMessage: true,
    channelType: ['text'],
  })
  async onPing(
    @Content() content: string,
    @Context() [context]: [Message],
  ): Promise<void> {
    try {
      this.pingService.ping(context.channel.id, content);
    } catch (error) {
      if (error instanceof MessageException) {
        await context.reply(error.message);
        return;
      }

      throw error;
    }
  }
}
