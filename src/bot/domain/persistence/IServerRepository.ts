import { Server } from '../models/Server';

export default interface IServerRepository {
  save(server: Server): Promise<void>;
  findById(id: string): Promise<Server | null>;
}
