import { Server } from '../models/Server';

export default interface Criteria {
  server?: Server;
  order?: number;
}
