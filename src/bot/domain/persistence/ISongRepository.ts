import { Song } from '../models/Song';

export default interface ISongRepository {
  save(song: Song): void;
  findById(id: string): Promise<Song | null>;
}
