import { Server } from '../models/Server';
import SongQueued from '../models/SongQueued';
import Criteria from './Criteria';

export default interface IQueueRepository {
  save(songQueued: SongQueued): Promise<void>;
  saveLast(songQueued: SongQueued): Promise<void>;
  find(criteria: Criteria, limit: number, asc: boolean): Promise<SongQueued[]>;
  delete(server: Server, order: number): Promise<void>;
}
