import { Song } from 'src/bot/domain/models/Song';
import { BaseEvent } from 'src/shared/domain/BaseEvent';

export class QueueEvent extends BaseEvent<Song[]> {
  static type = 'queue.event';

  constructor(
    private readonly queue: Array<Song>,
    public readonly serverId: string,
  ) {
    super();
  }
  getData(): Song[] {
    return this.queue;
  }
  getType(): string {
    return QueueEvent.type;
  }
}
