import { Server } from 'src/bot/domain/models/Server';
import { BaseEvent } from 'src/shared/domain/BaseEvent';

export class ServerEvent extends BaseEvent<Server> {
  static type = 'server.event';

  constructor(private readonly server: Server) {
    super();
  }
  getData(): Server {
    return this.server;
  }
  getType(): string {
    return ServerEvent.type;
  }
}
