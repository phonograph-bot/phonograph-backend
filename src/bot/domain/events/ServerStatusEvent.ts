import { BaseEvent } from '../../../shared/domain/BaseEvent';
import { ServerStatus } from '../models/ServerStatus';

export class ServerStatusEvent extends BaseEvent<ServerStatus> {
  static type = 'server.status';

  constructor(
    public readonly serverId: string,
    private serverStatus: ServerStatus,
  ) {
    super();
    if (!this.serverStatus.minute) {
      this.serverStatus.minute = 0;
    }
  }
  getData(): ServerStatus {
    return this.serverStatus;
  }
  getType(): string {
    return ServerStatusEvent.type;
  }
}
