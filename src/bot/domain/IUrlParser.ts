import { Song } from './models/Song';

export default interface UrlParser {
  supports(url: string): boolean;
  parse(url: string): Promise<Song>;
}
