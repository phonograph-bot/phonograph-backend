import { Readable } from 'node:stream';
import { Song } from './models/Song';

export interface ISongDownloader {
  supports(song: Song): boolean;
  getStream(song: Song): Readable;
}
