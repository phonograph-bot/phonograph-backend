import { ISongDownloader } from '../ISongDownloader';

export default function songDownloadersFactory(
  ...downloaders: ISongDownloader[]
) {
  return downloaders;
}
