export class ChannelNotFoundException extends Error {
  constructor(channelId?: string) {
    super(channelId ? `${channelId} not found` : null);
  }
}
