export class InvalidUrlException extends Error {
  constructor(url: string) {
    const newUrl = InvalidUrlException.processUrl(url);
    super(`${newUrl} is not valid`);
  }

  private static processUrl(url: string) {
    if (!url || url.length === 0) {
      return 'empty url';
    } else {
      return url;
    }
  }
}
