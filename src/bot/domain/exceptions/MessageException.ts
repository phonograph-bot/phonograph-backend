export class MessageException extends Error {
  constructor(message?: string) {
    super(message);
  }
}
