export class ServerNotFoundException extends Error {
  constructor(server?: string) {
    super(server ? `${server} not found` : null);
  }
}
