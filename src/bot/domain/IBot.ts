import { Readable } from 'node:stream';
import Message from './models/Message';
import { Server } from './models/Server';
import { ServerStatusType } from './models/ServerStatusType';

export default interface IBot {
  writeMessage(message: Message): void;
  checkChannel(channelId: string): boolean;
  checkServer(serverId: string): boolean;
  findServerByUserConnected(userId: string): string | null;
  findVoiceChannelByUserConnected(userId: string): string | null;
  getCurrentChannel(server: Server): string;
  setVolume(server: Server): void;
  pause(server: Server): void;
  resume(server: Server, onResume: () => void): void;
  play(
    stream: Readable,
    server: Server,
    onNextSong: () => void,
    onStart: () => void,
    onLoading: () => void,
    minute: number,
    channelId?: string,
  ): Promise<void>;
  getTime(server: Server): number;
  isConnected(server: Server): boolean;
  disconnect(server: Server): void;
  getStatus(server: Server): ServerStatusType;
}
