import { IEventPublisher } from 'src/shared/domain/IEventPublisher';
import { QueueEvent } from '../events/QueueEvent';
import { Server } from '../models/Server';
import { Song } from '../models/Song';
import SongQueued from '../models/SongQueued';
import IQueueRepository from '../persistence/IQueueRepository';
import QueueFinderService from './queue-finder.service';

export default class QueueAddService {
  private queueFinder: QueueFinderService;

  constructor(
    private queueRepository: IQueueRepository,
    private eventPublisher: IEventPublisher,
  ) {
    this.queueFinder = new QueueFinderService(queueRepository);
  }

  async add(song: Song, server: Server): Promise<void> {
    const songQueued = new SongQueued(null, server, song);
    await this.queueRepository.saveLast(songQueued);
    this.publish(server);
  }

  async addFirst(song: Song, server: Server) {
    const newSongQueued = new SongQueued(0, server, song);
    await this.queueRepository.save(newSongQueued);
    this.publish(server);
  }

  async publish(server: Server) {
    const queue = await this.queueFinder.find(server);
    const songs = queue.map((queued) => queued.song);
    const event = new QueueEvent(songs, server.id);

    this.eventPublisher.publish(event);
  }
}
