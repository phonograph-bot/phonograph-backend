import { ServerNotFoundException } from '../exceptions/ServerNotFoundException';
import IBot from '../IBot';
import { Server } from '../models/Server';
import IServerRepository from '../persistence/IServerRepository';

export default class ServerFinderService {
  constructor(private serverRepo: IServerRepository, private bot: IBot) {}

  async find(serverId: string): Promise<Server> {
    let server = await this.serverRepo.findById(serverId);

    if (!server) {
      if (!this.bot.checkServer(serverId)) {
        throw new ServerNotFoundException(serverId);
      }
      server = await this.create(serverId);
    }
    return server;
  }

  private async create(serverId: string): Promise<Server> {
    const server = Server.create(serverId, 10);
    await this.serverRepo.save(server);
    return server;
  }
}
