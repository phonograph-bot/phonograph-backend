import { IEventPublisher } from 'src/shared/domain/IEventPublisher';
import { QueueEvent } from '../events/QueueEvent';
import { Server } from '../models/Server';
import SongQueued from '../models/SongQueued';
import IQueueRepository from '../persistence/IQueueRepository';
import QueueFinderService from './queue-finder.service';

export default class QueueDeleterService {
  private queueFinder: QueueFinderService;

  constructor(
    private queueRepository: IQueueRepository,
    private eventPublisher: IEventPublisher,
  ) {
    this.queueFinder = new QueueFinderService(queueRepository);
  }

  async delete(songQueued: SongQueued) {
    await this.queueRepository.delete(songQueued.server, songQueued.order);
    this.publishEvent(songQueued.server);
  }

  async publishEvent(server: Server) {
    const queue = await this.queueFinder.find(server);
    const songs = queue.map((queueElement) => queueElement.song);
    const event = new QueueEvent(songs, server.id);

    this.eventPublisher.publish(event);
  }
}
