import { Readable } from 'node:stream';
import { ISongDownloader } from '../ISongDownloader';
import { Song } from '../models/Song';

export default class StreamService {
  constructor(private downloaders: ISongDownloader[]) {}

  getStream(song: Song): Readable | null {
    try {
      for (const parser of this.downloaders) {
        if (parser.supports(song)) {
          return parser.getStream(song);
        }
      }
    } catch (error) {
      return null;
    }
  }
}
