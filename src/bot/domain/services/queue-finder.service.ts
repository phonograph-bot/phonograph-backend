import { Server } from '../models/Server';
import SongQueued from '../models/SongQueued';
import IQueueRepository from '../persistence/IQueueRepository';

export default class QueueFinderService {
  private limit = 50;

  constructor(private queueRepository: IQueueRepository) {}

  async findFirst(server: Server): Promise<SongQueued | null> {
    return (await this.queueRepository.find({ server }, 1, true))[0];
  }

  find(server: Server): Promise<SongQueued[]> {
    return this.queueRepository.find({ server }, this.limit, true);
  }

  async findLast(server: Server): Promise<SongQueued | null> {
    return (await this.queueRepository.find({ server }, 1, false))[0];
  }
}
