import { Server } from '../models/Server';
import IServerRepository from '../persistence/IServerRepository';

export default class ServerUpdaterService {
  constructor(private serverRepo: IServerRepository) {}

  async update(server: Server): Promise<void> {
    await this.serverRepo.save(server);
  }
}
