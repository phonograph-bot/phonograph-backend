import { Injectable } from '@nestjs/common';
import { InvalidUrlException } from '../exceptions/InvalidUrlException';
import IUrlParser from '../IUrlParser';
import { Song } from '../models/Song';

@Injectable()
export default class UrlParserService {
  constructor(private parsers: IUrlParser[]) {}

  parse(url: string): Promise<Song> {
    try {
      for (const parser of this.parsers) {
        if (parser.supports(url)) {
          return parser.parse(url);
        }
      }
    } catch (error) {}

    throw new InvalidUrlException(url);
  }
}
