import IBot from '../IBot';
import QueueDeleterService from './queue-deleter.service';
import QueueFinderService from './queue-finder.service';
import StreamService from './stream.service';
import { Server } from '../models/Server';
import { IEventPublisher } from 'src/shared/domain/IEventPublisher';
import { ServerStatusEvent } from 'src/bot/domain/events/ServerStatusEvent';
import { ServerStatusType } from '../models/ServerStatusType';
import SongQueued from '../models/SongQueued';

export default class BotPlayService {
  constructor(
    private bot: IBot,
    private streamService: StreamService,
    private queueFinder: QueueFinderService,
    private queueDelete: QueueDeleterService,
    private eventPublisher: IEventPublisher,
  ) {}

  async play(server: Server, channelId?: string, minute = 0): Promise<void> {
    this.publish(server, ServerStatusType.Loading, minute);

    const nextSong = await this.queueFinder.findFirst(server);
    if (!nextSong) {
      this.publish(server, ServerStatusType.Stopped, minute);
      return this.bot.disconnect(server);
    }

    const stream = this.streamService.getStream(nextSong.song);
    if (!stream) {
      this.publish(server, ServerStatusType.Stopped, minute);
      await this.queueDelete.delete(nextSong);
      return this.play(server, channelId);
    }

    this.bot.play(
      stream,
      server,
      () => {
        this.onNextSong(server, nextSong);
      },
      () => {
        this.onStart(server, minute);
      },
      () => {
        this.publish(server, ServerStatusType.Loading, minute);
      },
      minute,
      channelId,
    );
  }

  private async onNextSong(server: Server, song: SongQueued) {
    await this.queueDelete.delete(song);
    return this.play(server);
  }

  private async onStart(server: Server, minute: number) {
    this.publish(server, ServerStatusType.Playing, minute);
  }

  private publish(server: Server, status: ServerStatusType, minute = 0) {
    const statusEvent = new ServerStatusEvent(server.id, {
      minute,
      status,
    });
    this.eventPublisher.publish(statusEvent);
  }
}
