import { Injectable } from '@nestjs/common';
import { ChannelNotFoundException } from '../exceptions/ChannelNotFoundException';
import IBot from '../IBot';

@Injectable()
export default class ChannelCheckerService {
  check(bot: IBot, channelId: string) {
    if (!bot.checkChannel(channelId)) {
      throw new ChannelNotFoundException(channelId);
    }
  }
}
