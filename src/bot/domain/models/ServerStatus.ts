import { ServerStatusType } from './ServerStatusType';

export interface ServerStatus {
  status: ServerStatusType;
  minute?: number;
}
