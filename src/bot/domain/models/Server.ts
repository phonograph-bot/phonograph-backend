import { ServerException } from '../exceptions/ServerException';
export class Server {
  constructor(private _id: string, private _volume: number) {}

  static create(id: string, volume: number) {
    if (isNaN(volume) || volume <= 0 || volume > 100) {
      throw new ServerException('Volume invalid');
    }

    return new Server(id, volume);
  }

  get id(): string {
    return this._id;
  }

  get volume(): number {
    return this._volume;
  }

  set volume(volume: number) {
    if (isNaN(volume) || volume <= 0 || volume > 100) {
      throw new ServerException('Volume invalid');
    }
    this._volume = volume;
  }
}
