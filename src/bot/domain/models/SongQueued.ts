import { Server } from './Server';
import { Song } from './Song';

export default class SongQueued {
  // create namedconstructor
  constructor(public order: number, public server: Server, public song: Song) {}
}
