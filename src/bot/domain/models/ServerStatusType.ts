export enum ServerStatusType {
  Paused = 'PAUSED',
  Playing = 'PLAYING',
  Stopped = 'STOPPED',
  Loading = 'LOADING',
}
