import { MessageException } from '../exceptions/MessageException';

export default class Message {
  private _channelId: string | number;
  private _content: string;

  constructor(channelId: string | number, message: string) {
    this._channelId = channelId;
    this._content = message;
  }

  get channelId(): string | number {
    return this._channelId;
  }

  get content(): string {
    return this._content;
  }

  static create(channelId: string | number, message: string): Message {
    if (channelId == null) {
      throw new MessageException('channelId is null');
    }

    if (message == null) {
      throw new MessageException('message is null');
    }

    if (message.length === 0) {
      throw new MessageException('message is empty');
    }

    return new Message(channelId, message);
  }
}
