import { InvalidUrlException } from '../exceptions/InvalidUrlException';
import { SongException } from '../exceptions/SongException';
import { SongType } from './SongType';

export class Song {
  private static URL_REGEX = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

  private _url: string;
  private _type: SongType;
  private _minute: number;
  private _title: string;
  private _author: string;
  private _image: string;

  constructor(
    url: string,
    type: SongType,
    minute: number,
    title: string,
    author: string,
    image: string,
  ) {
    this._url = url;
    this._type = type;
    this._minute = minute;
    this._title = title;
    this._author = author;
    this._image = image;
  }

  static create(
    url: string,
    type: SongType,
    minute: number,
    title: string,
    author: string,
    image: string,
  ) {
    if (!Song.URL_REGEX.test(url)) {
      throw new InvalidUrlException(url);
    }

    if (!type) {
      throw new SongException('Invalid Type');
    }

    if (isNaN(minute)) {
      throw new SongException('Invalid Minute');
    }

    return new Song(url, type, minute, title, author, image);
  }

  get url(): string {
    return this._url;
  }

  get type(): SongType {
    return this._type;
  }

  get minute(): number {
    return this._minute;
  }

  get title(): string {
    return this._title;
  }

  get author(): string {
    return this._author;
  }

  get image(): string {
    return this._image;
  }
}
