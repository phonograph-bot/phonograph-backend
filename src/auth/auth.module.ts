import { HttpModule, Module } from '@nestjs/common';
import { AuthController } from './infrastructure/controllers/auth.controller';
import DiscordStrategy from './infrastructure/strategies/discord.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtImpl } from './infrastructure/JwtImpl';
import { TokenCreatorService } from './application/token-creator.service';
import { JwtStrategy } from './infrastructure/strategies/jwt.strategy';

@Module({
  providers: [
    DiscordStrategy,
    JwtStrategy,
    {
      provide: 'JWT',
      useClass: JwtImpl,
    },
    TokenCreatorService,
  ],
  controllers: [AuthController],
  imports: [
    HttpModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '7d' },
    }),
  ],
  exports: [
    {
      provide: 'JWT',
      useClass: JwtImpl,
    },
  ],
})
export class AuthModule {}
