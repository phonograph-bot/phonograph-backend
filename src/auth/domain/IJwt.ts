import { TokenData } from './TokenData';

export interface IJwt {
  sign(data: TokenData): string;
  decode(token: string): TokenData;
}
