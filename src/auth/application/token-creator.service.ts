import { Inject, Injectable } from '@nestjs/common';
import { IJwt } from '../domain/IJwt';
import { TokenData } from '../domain/TokenData';

@Injectable()
export class TokenCreatorService {
  constructor(@Inject('JWT') private readonly jwtService: IJwt) {}

  create(tokenData: TokenData) {
    return this.jwtService.sign(tokenData);
  }
}
