import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenCreatorService } from '../../application/token-creator.service';

@Controller()
export class AuthController {
  constructor(private jwtService: TokenCreatorService) {}

  @Get('auth/discord/callback')
  @UseGuards(AuthGuard('discord'))
  callback(@Req() req) {
    return {
      token: this.jwtService.create(req.user),
    };
  }
}
