import { HttpService, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-oauth2';
@Injectable()
export default class DiscordStrategy extends PassportStrategy(
  Strategy,
  'discord',
) {
  constructor(private http: HttpService) {
    super({
      clientID: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      callbackURL: process.env.CALLBACK_URL,
      scope: ['identify'],
      authorizationURL: 'https://discord.com/api/oauth2/authorize',
      tokenURL: 'https://discordapp.com/api/oauth2/token',
    });
  }

  async validate(accessToken: string): Promise<any> {
    return await this.http
      .get('https://discordapp.com/api/users/@me', {
        headers: { Authorization: `Bearer ${accessToken}` },
      })
      .toPromise()
      .then((response) => ({
        user: response.data,
      }));
  }
}
