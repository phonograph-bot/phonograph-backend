import { IJwt } from '../domain/IJwt';
import { TokenData } from '../domain/TokenData';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtImpl implements IJwt {
  constructor(private readonly jwtService: JwtService) {}

  sign(data: TokenData): string {
    return this.jwtService.sign(data, {
      secret: process.env.JWT_SECRET,
      expiresIn: '7d',
    });
  }
  decode(token: string): TokenData {
    return this.jwtService.verify(token, {
      secret: process.env.JWT_SECRET,
    });
  }
}
