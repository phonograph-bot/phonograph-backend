import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { BotModule } from 'src/bot/bot.module';
import { PauseWsService } from './application/pause-ws.service';
import { PlayWsService } from './application/play-ws.service';
import { ResumeWsService } from './application/resume-ws.service';
import { RoomJoinService } from './application/room-join.service';
import { SkipWsService } from './application/skip.service';
import { TimeWsService } from './application/time-ws.service';
import { VolWsService } from './application/vol-ws.service';
import { QueueHandler } from './infrastructure/eventsHandler/queue.handler';
import { ServerHandler } from './infrastructure/eventsHandler/server.handler';
import { ServerStatusHandler } from './infrastructure/eventsHandler/serverStatus.handler';
import { JoinServerGateway } from './infrastructure/gateway/joinServer.gateway';
import { PauseGateway } from './infrastructure/gateway/pause.gateway';
import { PlayGateway } from './infrastructure/gateway/play.gateway';
import { ResumeGateway } from './infrastructure/gateway/resume.gateway';
import { SkipGateway } from './infrastructure/gateway/skip.gateway';
import { TimeGateway } from './infrastructure/gateway/time.gateway';
import { VolGateway } from './infrastructure/gateway/vol.gateway';
import { RoomManagerSocketIO } from './infrastructure/RoomManagerSocketIO';
import { AddGateway } from './infrastructure/gateway/add.gateway';
import { AddWsService } from './application/add-ws.service';

@Module({
  providers: [
    JoinServerGateway,
    RoomJoinService,
    SkipGateway,
    SkipWsService,
    PauseGateway,
    PauseWsService,
    ResumeGateway,
    ResumeWsService,
    PlayGateway,
    PlayWsService,
    VolGateway,
    VolWsService,
    TimeGateway,
    TimeWsService,
    AddGateway,
    AddWsService,
    {
      provide: 'RoomManager',
      useClass: RoomManagerSocketIO,
    },
    ServerHandler,
    QueueHandler,
    ServerStatusHandler,
  ],
  imports: [BotModule, AuthModule],
})
export class WsModule {}
