import { Inject, Injectable } from '@nestjs/common';
import IBot from 'src/bot/domain/IBot';
import { PlayUrlService } from '../../bot/application/services/play-url.service';

@Injectable()
export class AddWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly playUrlService: PlayUrlService,
  ) {}

  add(userId: string, url: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    const channelId = this.bot.findVoiceChannelByUserConnected(userId);
    return this.playUrlService.play(url, channelId, serverId);
  }
}
