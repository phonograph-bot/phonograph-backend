import { Inject, Injectable } from '@nestjs/common';
import { PauseService } from 'src/bot/application/services/pause.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class PauseWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly pauseService: PauseService,
  ) {}

  pause(userId: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    return this.pauseService.pause(serverId);
  }
}
