import { Inject, Injectable } from '@nestjs/common';
import { PlayService } from 'src/bot/application/services/play.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class PlayWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly playService: PlayService,
  ) {}

  play(userId: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    const channelId = this.bot.findVoiceChannelByUserConnected(userId);

    return this.playService.play(channelId, serverId);
  }
}
