import { Inject, Injectable } from '@nestjs/common';
import VolumeService from 'src/bot/application/services/volume.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class VolWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly volumeService: VolumeService,
  ) {}

  async vol(userId: string, vol: number): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    await this.volumeService.setVolume(serverId, vol);
  }
}
