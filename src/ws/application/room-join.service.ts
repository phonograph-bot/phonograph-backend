import { Inject, Injectable } from '@nestjs/common';
import { ServerStatus } from 'src/bot/domain/models/ServerStatus';
import IBot from 'src/bot/domain/IBot';
import IQueueRepository from 'src/bot/domain/persistence/IQueueRepository';
import IServerRepository from 'src/bot/domain/persistence/IServerRepository';
import QueueFinderService from 'src/bot/domain/services/queue-finder.service';
import ServerFinderService from 'src/bot/domain/services/server-finder.service';
import { IRoomManager } from '../domain/IRoomManager';
import { Server } from 'src/bot/domain/models/Server';
import { Namespaces } from '../infrastructure/Namespaces';

@Injectable()
export class RoomJoinService {
  private serverFinder: ServerFinderService;
  private queueFinder: QueueFinderService;

  constructor(
    @Inject('BOT') private bot: IBot,
    @Inject('RoomManager') private readonly roomManager: IRoomManager,
    @Inject('ServerRepository') serverRepository: IServerRepository,
    @Inject('QueueRepository') queueRepository: IQueueRepository,
  ) {
    this.serverFinder = new ServerFinderService(serverRepository, bot);
    this.queueFinder = new QueueFinderService(queueRepository);
  }

  async join(userId: string, socketId: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);

    if (serverId) {
      const server = await this.serverFinder.find(serverId);
      const queue = await this.queueFinder.find(server);
      const serverStatus = this.getServerStatus(server);

      const songs = queue.map((queued) => queued.song);

      await this.roomManager.joinOne(userId, serverId, socketId);
      this.roomManager.emit(serverId, Namespaces.Server, server);
      this.roomManager.emit(serverId, Namespaces.Queue, songs);
      this.roomManager.emit(serverId, Namespaces.ServerStatus, serverStatus);
    }
  }

  private getServerStatus(server: Server): ServerStatus {
    const type = this.bot.getStatus(server);
    const minute = this.bot.getTime(server);

    return {
      status: type,
      minute,
    };
  }
}
