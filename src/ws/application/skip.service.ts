import { Inject, Injectable } from '@nestjs/common';
import { SkipService as SkipBotService } from 'src/bot/application/services/skip.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class SkipWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly skipBotService: SkipBotService,
  ) {}

  skip(userId: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    return this.skipBotService.skip(serverId);
  }
}
