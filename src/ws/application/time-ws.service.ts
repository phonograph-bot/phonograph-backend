import { Inject, Injectable } from '@nestjs/common';
import { TimeService } from 'src/bot/application/services/time.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class TimeWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly timeService: TimeService,
  ) {}

  setTime(userId: string, time: number): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    return this.timeService.setTime(serverId, time);
  }
}
