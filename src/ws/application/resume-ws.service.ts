import { Inject, Injectable } from '@nestjs/common';
import { ResumeService } from 'src/bot/application/services/resume.service';
import IBot from 'src/bot/domain/IBot';

@Injectable()
export class ResumeWsService {
  constructor(
    @Inject('BOT') private readonly bot: IBot,
    private readonly resumeBotService: ResumeService,
  ) {}

  resume(userId: string): Promise<void> {
    const serverId = this.bot.findServerByUserConnected(userId);
    return this.resumeBotService.resume(serverId);
  }
}
