import { ServerStatusType } from 'src/bot/domain/models/ServerStatusType';

export interface ServerStatusDTO {
  minute?: number;
  status: ServerStatusType;
}
