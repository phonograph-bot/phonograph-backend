import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { IRoomManager } from '../domain/IRoomManager';

@WebSocketGateway()
export class RoomManagerSocketIO implements IRoomManager {
  private userSockets = new Map<string, string>();

  @WebSocketServer() private wss: Server;

  emit(serverId: string, namespace: string, data: any) {
    this.wss.to(serverId).emit(namespace, data);
  }

  async joinOne(
    userId: string,
    serverId: string,
    socketId?: string,
  ): Promise<void> {
    if (!socketId) {
      socketId = this.userSockets.get(userId);
    }

    if (!socketId) {
      return;
    }

    await this.leaveAll(userId, socketId);

    return new Promise((resolve) => {
      this.userSockets.set(userId, socketId);
      this.wss.sockets.sockets[socketId].join(serverId, () => resolve());
    });
  }

  leaveAll(userId: string, socketId?: string): Promise<void> {
    if (!socketId) {
      socketId = this.userSockets.get(userId);
    }

    if (!socketId) {
      return;
    }

    return new Promise((resolve) => {
      this.userSockets.delete(userId);
      this.wss.sockets.sockets[socketId].leaveAll();
      resolve();
    });
  }

  getServerRoomId(userId: string): string {
    return this.userSockets.get(userId);
  }
}
