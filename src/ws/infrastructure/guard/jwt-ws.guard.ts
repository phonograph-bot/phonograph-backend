import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Socket } from 'socket.io';
import { Observable } from 'rxjs';
import { IJwt } from 'src/auth/domain/IJwt';

@Injectable()
export class JwtWsGuard implements CanActivate {
  constructor(@Inject('JWT') private readonly jwt: IJwt) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const client: Socket = context.switchToWs().getClient();
    const token = client.handshake.query.token as string;
    let tokenData;

    try {
      tokenData = this.jwt.decode(token);
    } catch (error) {
      return false;
    }

    client['decoded_token'] = tokenData;
    return true;
  }
}
