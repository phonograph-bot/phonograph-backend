export enum Namespaces {
  Queue = 'queue',
  ServerStatus = 'serverStatus',
  Server = 'server',
  Skip = 'skip',
  Play = 'play',
  Resume = 'resume',
  Pause = 'pause',
  Vol = 'vol',
  Time = 'time',
  Add = 'add',
}
