import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { TokenData } from 'src/auth/domain/TokenData';

export const Token = createParamDecorator((data, ctx: ExecutionContext) => {
  return ctx.switchToWs().getClient()['decoded_token'] as TokenData;
});
