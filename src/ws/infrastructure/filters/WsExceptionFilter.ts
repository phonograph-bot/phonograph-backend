import { Catch, ArgumentsHost } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
import { ChannelNotFoundException } from '../../../bot/domain/exceptions/ChannelNotFoundException';
import { InvalidUrlException } from '../../../bot/domain/exceptions/InvalidUrlException';
import { MessageException } from '../../../bot/domain/exceptions/MessageException';
import { ServerException } from '../../../bot/domain/exceptions/ServerException';
import { ServerNotFoundException } from '../../../bot/domain/exceptions/ServerNotFoundException';
import { SongException } from '../../../bot/domain/exceptions/SongException';

@Catch()
export class WsExceptionFilter extends BaseWsExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    // TODO: send to client
    if (exception instanceof ChannelNotFoundException) return;
    if (exception instanceof InvalidUrlException) return;
    if (exception instanceof MessageException) return;
    if (exception instanceof ServerException) return;
    if (exception instanceof ServerNotFoundException) return;
    if (exception instanceof SongException) return;

    super.catch(exception, host);
  }
}
