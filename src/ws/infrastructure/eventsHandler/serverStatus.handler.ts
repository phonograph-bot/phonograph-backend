import { OnEvent } from '@nestjs/event-emitter';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server as ServerIO } from 'socket.io';
import { ServerStatusEvent } from 'src/bot/domain/events/ServerStatusEvent';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';

@WebSocketGateway()
export class ServerStatusHandler {
  @WebSocketServer() wss: ServerIO;

  @OnEvent(ServerStatusEvent.type)
  handleServerStatus(event: ServerStatusEvent) {
    const data = event.getData();
    this.wss.to(event.serverId).emit(Namespaces.ServerStatus, data);
  }
}
