import { OnEvent } from '@nestjs/event-emitter';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server as ServerIO } from 'socket.io';
import { QueueEvent } from 'src/bot/domain/events/QueueEvent';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';

@WebSocketGateway()
export class QueueHandler {
  @WebSocketServer() wss: ServerIO;

  @OnEvent(QueueEvent.type)
  handleQueueEvent(event: QueueEvent) {
    const data = event.getData();
    this.wss.to(event.serverId).emit(Namespaces.Queue, data);
  }
}
