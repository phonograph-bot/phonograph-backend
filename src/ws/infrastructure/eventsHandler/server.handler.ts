import { OnEvent } from '@nestjs/event-emitter';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server as ServerIO } from 'socket.io';
import { ServerEvent } from 'src/bot/domain/events/ServerEvent';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';

@WebSocketGateway()
export class ServerHandler {
  @WebSocketServer() wss: ServerIO;

  @OnEvent(ServerEvent.type)
  handleServerEvent(event: ServerEvent) {
    const server = event.getData();
    this.wss.to(server.id).emit(Namespaces.Server, server);
  }
}
