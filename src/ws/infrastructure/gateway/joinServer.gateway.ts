import { UseFilters, UseGuards } from '@nestjs/common';
import {
  ConnectedSocket,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { RoomJoinService } from 'src/ws/application/room-join.service';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class JoinServerGateway {
  @WebSocketServer() wss: Server;

  constructor(private roomJoinService: RoomJoinService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage('joinServer')
  @UseFilters(new WsExceptionFilter())
  async handleEventJoinServer(
    @ConnectedSocket() client: Socket,
    @Token() token: TokenData,
  ): Promise<void> {
    return this.roomJoinService.join(token.user.id, client.id);
  }
}
