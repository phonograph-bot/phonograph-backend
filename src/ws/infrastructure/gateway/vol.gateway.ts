import { UseFilters, UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { VolWsService } from 'src/ws/application/vol-ws.service';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class VolGateway {
  @WebSocketServer() wss: Server;

  constructor(private volWsService: VolWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Vol)
  @UseFilters(new WsExceptionFilter())
  vol(@Token() token: TokenData, @MessageBody() data: number): Promise<void> {
    return this.volWsService.vol(token.user.id, data);
  }
}
