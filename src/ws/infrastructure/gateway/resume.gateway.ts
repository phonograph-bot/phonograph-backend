import { UseFilters, UseGuards } from '@nestjs/common';
import { SubscribeMessage, WebSocketServer } from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { ResumeWsService } from 'src/ws/application/resume-ws.service';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class ResumeGateway {
  @WebSocketServer() wss: Server;

  constructor(private resumeWsService: ResumeWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Resume)
  @UseFilters(new WsExceptionFilter())
  resume(@Token() token: TokenData): Promise<void> {
    return this.resumeWsService.resume(token.user.id);
  }
}
