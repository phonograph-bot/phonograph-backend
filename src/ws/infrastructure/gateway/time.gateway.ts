import { UseFilters, UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { TimeWsService } from 'src/ws/application/time-ws.service';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class TimeGateway {
  @WebSocketServer() wss: Server;

  constructor(private timeWsService: TimeWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Time)
  @UseFilters(new WsExceptionFilter())
  seek(@Token() token: TokenData, @MessageBody() data: number): Promise<void> {
    return this.timeWsService.setTime(token.user.id, data);
  }
}
