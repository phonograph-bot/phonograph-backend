import { UseFilters, UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { AddWsService } from '../../application/add-ws.service';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class AddGateway {
  @WebSocketServer() wss: Server;

  constructor(private addWsService: AddWsService) {}

  @UseFilters(new WsExceptionFilter())
  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Add)
  add(@Token() token: TokenData, @MessageBody() data: string): Promise<void> {
    return this.addWsService.add(token.user.id, data);
  }
}
