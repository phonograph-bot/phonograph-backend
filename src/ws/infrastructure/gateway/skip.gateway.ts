import { UseFilters, UseGuards } from '@nestjs/common';
import { SubscribeMessage, WebSocketServer } from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { SkipWsService } from 'src/ws/application/skip.service';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class SkipGateway {
  @WebSocketServer() wss: Server;

  constructor(private skipService: SkipWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage('skip')
  @UseFilters(new WsExceptionFilter())
  skip(@Token() token: TokenData): Promise<void> {
    return this.skipService.skip(token.user.id);
  }
}
