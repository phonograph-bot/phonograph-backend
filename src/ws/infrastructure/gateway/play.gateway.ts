import { UseFilters, UseGuards } from '@nestjs/common';
import { SubscribeMessage, WebSocketServer } from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { PlayWsService } from 'src/ws/application/play-ws.service';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class PlayGateway {
  @WebSocketServer() wss: Server;

  constructor(private playWsService: PlayWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Play)
  @UseFilters(new WsExceptionFilter())
  play(@Token() token: TokenData): Promise<void> {
    return this.playWsService.play(token.user.id);
  }
}
