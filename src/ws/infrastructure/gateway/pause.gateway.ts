import { UseFilters, UseGuards } from '@nestjs/common';
import { SubscribeMessage, WebSocketServer } from '@nestjs/websockets';
import { WebSocketGateway } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { TokenData } from 'src/auth/domain/TokenData';
import { JwtWsGuard } from '../guard/jwt-ws.guard';
import { Token } from '../decorator/token.decorator';
import { PauseWsService } from 'src/ws/application/pause-ws.service';
import { Namespaces } from 'src/ws/infrastructure/Namespaces';
import { WsExceptionFilter } from '../filters/WsExceptionFilter';

@WebSocketGateway()
export class PauseGateway {
  @WebSocketServer() wss: Server;

  constructor(private pauseWsService: PauseWsService) {}

  @UseGuards(JwtWsGuard)
  @SubscribeMessage(Namespaces.Pause)
  @UseFilters(new WsExceptionFilter())
  pause(@Token() token: TokenData): Promise<void> {
    return this.pauseWsService.pause(token.user.id);
  }
}
