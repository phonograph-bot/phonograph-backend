import { Server } from 'src/bot/domain/models/Server';
import { ServerStatus } from 'src/bot/domain/models/ServerStatus';
import { Song } from 'src/bot/domain/models/Song';

export class ServerData {
  constructor(
    public readonly queue: Song[],
    public readonly serverStatus: ServerStatus,
    public readonly server?: Server,
  ) {}
}
