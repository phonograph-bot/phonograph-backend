export interface IRoomManager {
  joinOne(userId: string, serverId: string, socketId?: string): Promise<void>;
  leaveAll(userId: string, socketId?: string): Promise<void>;
  getServerRoomId(userId: string): string;
  emit(serverId: string, namespace: string, data: any);
}
