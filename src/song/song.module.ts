import { Module } from '@nestjs/common';
import { SongController } from './infrastructure/song.controller';
import { SongFinderService } from './application/song-finder.service';
import { YoutubeNameFinder } from './infrastructure/YoutubeNameFinder';

@Module({
  controllers: [SongController],
  providers: [
    SongFinderService,
    {
      provide: 'YoutubeNameFinder',
      useClass: YoutubeNameFinder,
    },
  ],
})
export class SongModule {}
