import { Inject, Injectable } from '@nestjs/common';
import { Song } from '../../bot/domain/models/Song';
import { NameFinder } from '../domain/NameFinder';

@Injectable()
export class SongFinderService {
  constructor(
    @Inject('YoutubeNameFinder') private readonly nameFinder: NameFinder,
  ) {}

  findByName(name = 'music'): Promise<Song[]> {
    return this.nameFinder.parse(name);
  }
}
