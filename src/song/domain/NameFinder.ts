import { Song } from '../../bot/domain/models/Song';

export interface NameFinder {
  parse(name: string): Promise<Song[]>;
}
