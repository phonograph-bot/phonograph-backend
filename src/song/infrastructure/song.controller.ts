import { Controller, Get, Query } from '@nestjs/common';
import { SongFinderService } from '../application/song-finder.service';
import { Song } from '../../bot/domain/models/Song';

@Controller('song')
export class SongController {
  constructor(private readonly songFinder: SongFinderService) {}

  @Get()
  getSongs(@Query('name') name: string): Promise<Song[]> {
    return this.songFinder.findByName(name);
  }
}
