import { NameFinder } from '../domain/NameFinder';
import * as yts from 'ytsr';
import { Song } from '../../bot/domain/models/Song';
import { Injectable } from '@nestjs/common';
import { SongType } from '../../bot/domain/models/SongType';
import { Video } from 'ytsr';

@Injectable()
export class YoutubeNameFinder implements NameFinder {
  async parse(name: string): Promise<Song[]> {
    if (!name || name.length === 0) {
      return [];
    }

    const songList = await yts(name, {
      limit: 4,
    });
    return songList.items
      .filter((item) => (item.type = 'video'))
      .filter((item) => !item['isLive'])
      .filter((item) => !item['isUpcoming'])
      .filter((item) => item['duration'])
      .map((item) => item as Video)
      .map((song) => {
        return new Song(
          song.url,
          SongType.Youtube,
          this.parseDuration(song.duration),
          song.title,
          song?.author?.name || 'Unknown',
          song.thumbnails[0].url,
        );
      });
  }

  private parseDuration(duration: string) {
    const parts = duration.split(':').reverse();
    const seconds = parseInt(parts[0]);
    const minutes = parseInt(parts[1]);
    const hours = parseInt(parts[2]);

    let time = seconds;
    if (minutes) time += minutes * 60;
    if (hours) time += hours * 3600;

    return time;
  }
}
