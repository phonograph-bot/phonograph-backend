import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from 'eventemitter2';
import { BaseEvent } from '../domain/BaseEvent';
import { IEventPublisher } from '../domain/IEventPublisher';

@Injectable()
export class EventPublisher implements IEventPublisher {
  constructor(private readonly eventEmiter: EventEmitter2) {}

  publish<T>(event: BaseEvent<T>): void {
    this.eventEmiter.emit(event.getType(), event);
  }
}
