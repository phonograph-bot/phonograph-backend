export abstract class BaseEvent<T> {
  abstract getType(): string;
  abstract getData(): T;
}
