import { BaseEvent } from './BaseEvent';

export interface IEventPublisher {
  publish<T>(event: BaseEvent<T>): void;
}
