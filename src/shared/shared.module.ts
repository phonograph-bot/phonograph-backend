import { Module } from '@nestjs/common';
import { EventPublisher } from './infrastructure/EventPublisher';

@Module({
  providers: [
    {
      provide: 'EVENT',
      useClass: EventPublisher,
    },
  ],
  exports: [
    {
      provide: 'EVENT',
      useClass: EventPublisher,
    },
  ],
})
export class SharedModule {}
